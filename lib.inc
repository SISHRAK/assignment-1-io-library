section .text

%define _EXIT_SYSCALL 60
%define STDOUT 1
; Принимает код возврата и завершает текущий процесс

exit:
    mov rax, _EXIT_SYSCALL
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; Ready
string_length:
    xor rax, rax
    mov rax, -1
.loop:
    inc rax
    cmp byte [rdi+rax], 0
    jne .loop
    ret

; Выводит строку, начиная с адреса в rsi, в stdout
; Ready
print_string:
    push rdi           
    call string_length ; Вызываем функцию string_length для подсчета длины строки
    pop rsi            
    mov rdx, rax       
    mov rax, STDOUT         
    mov rdi, STDOUT         
    syscall            
    ret                

; Выводит символ, код которого находится в rdi, в stdout
; Ready
print_char:
    push rdi           ; Сохраняем rdi на стеке
    mov rsi, rsp       ; Устанавливаем указатель на символ в rsi
    mov rax, STDOUT         
    mov rdi, STDOUT         
    mov rdx, STDOUT         ; Устанавливаем количество байт для вывода (1 символ)
    syscall            ; Вызываем системный вызов для вывода символа
    pop rdi            
    ret               

; Выводит символ новой строки (LF, 0xA) в stdout
; Ready
print_newline:
    mov rdi, 0xA  
    syscall    
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Ready
print_uint:   
    mov r8, rdi        
    mov r10, rsp       ; Сохраняем начальное значение указателя стека для определения конца вывода
    sub rsp, 32        ; Место для хранения 
    dec r10            ; Указываем на нулевой терминатор
    mov r9, 10         ; Устанавливаем делитель в r9
    mov byte[r10], 0x0
.loop:
    mov rax, rdi       
    xor rdx, rdx       
    div r9             
    mov r8, rax        
    add rdx, '0'       ; Преобразуем остаток 
    dec r10            
    mov [r10], dl      ; Сохраняем в стеке

    mov rdi, r8        ; Восстанавливаем остаток
    test rdi, rdi      ; В конце ли числа
    jnz .loop       
    mov rdi, r10       ; Устанавливаем rdi на начало строки на стеке
    call print_string  ; Вызываем функцию для вывода строки
    add rsp, 32        
    ret   

; Выводит знаковое 8-байтовое число в десятичном формате
; Ready
print_int:
    cmp rdi, 0         ; Сравниваем rdi с нулем
    jge .positive      ; Если rdi неотрицательное, перейти к .positive
    push rdi           ; Сохраняем rdi на стеке
    mov rdi, '-'       ; Устанавливаем rdi в код символа "-"
    call print_char    ; Вызываем функцию print_char для вывода "-"
    pop rdi            ; Восстанавливаем исходное значение rdi
    neg rdi            ; Инвертируем знак числа 
.positive:
    jmp print_uint    
    


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; Ready
string_equals:
    mov rax, 0           
.loop:
    mov dl, byte [rdi]   ; Загружаем байт из первой строки
    cmp dl, byte [rsi]   ; Сравниваем с байтом из второй строки
    jne .not_equal       ; Если не совпадают переходим к .not_equal

    test dl, dl          
    je .equal            ; Если обе строки завершаются, считаем их равными
    inc rdi                  
    inc rsi              
    jmp .loop            

.equal:
    inc rax              
.not_equal:
    ret                  


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; Ready
read_char:
    mov rax, 0
    push 0        
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax         ; Извлекаем символ из стека и помещаем в rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; Ready
read_word:
    xor rax, rax
    xor rdx, rdx  
    mov r14, 0
    push r14
    push r13
    push r12     
    mov r12, rdi             
    mov r13, rsi                
    dec r13                  
.spaces_loop:     
    call read_char
    cmp rax, 0x20             ; Если ' ', '\t', '\n' - скипаем
    je  .spaces_loop
    cmp rax, 0x9
    je  .spaces_loop
    cmp rax, 0xA
    je  .spaces_loop
.loop:                     ; Бегаем по символам
    cmp rax, 0xA           ; Если ' ', '\t', '\n' - скипаем
    je  .final
    cmp rax, 0x9
    je  .final
    cmp rax, 0x20
    je  .final
    cmp r14, r13             
    jg  .err
    test rax, rax
    je .final
    mov [r12 + r14], rax     
    inc r14
    call read_char
    jmp .loop
.final:
    mov rdx, r14            
    mov byte [r12 + r14], 0
    mov rax, r12
    jmp .end
.err:
    xor rdx, rdx
    xor rax, rax
.end:
    pop r12
    pop r13
    pop r14
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; Ready
parse_uint:
    xor r8, r8          
    mov rsi, 10       ; На что делим
    mov rax, 0
    mov rdx, 0
    mov rcx, 0
    cmp byte[rdi], 0   ; Строка пустая?
    je .finally
.loop:
    mov al, byte[rdi + rcx]
    cmp al, '0'
    jb .finally
    cmp al, '9'
    ja .finally

    sub al, '0'
    push rax
    mov rax, r8
    mul rsi
    mov r8, rax
    pop rax

    add r8, rax
    inc rcx
    jmp .loop

.finally:
    mov rdx, rcx
    mov rax, r8  
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Ready
parse_int:
    xor rax, rax
    cmp byte [rdi], '-' ; проверка на знак перед числом
    jne .positive

.negative:
    inc rdi         ; добавили знак для значения длинны
    call parse_uint
    inc rdx
    neg rax         ; меняем число на противоположное
    ret
.positive:
    jmp parse_uint


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Ready
string_copy:
    push r12 
    inc rax
    cmp rax, rdx           
    jg .err                ; Если строка не помещается в буфер, перейти к обработке ошибки
    mov rcx, 0        
    xor r12, r12
.loop:
    mov r12b, [rdi+rcx]      
    mov [rsi+rcx], r12      
    inc rcx                
    cmp byte [rdi+rcx], 0  
    jnz .loop              
    pop r12
    ret
.err:
    xor rax, rax           ; Возвращаем 0 в rax, чтобы указать об ошибке
    pop r12
    ret
